package com.foodiewoodie.constant;

/**
 * Created by Praveen on 1/22/2017.
 */

public interface Constants {
    int SPLASH_TIMEOUT = 3 * 1000;
    long NETWORK_DUMMY_WAIT_TIME = 8 * 1000;
    String HEADER_TEXT = "header";
    String DATA = "data";
    String SNACKBAR_SHOW = "snackbar";
}
