package com.foodiewoodie.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.foodiewoodie.models.RecipeModel;

import java.util.ArrayList;


public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int STATUS_AVAILABLE = 0;
    private static final int STATUS_UNAVAILABLE = 1;

    // Logcat tag
    private static final String LOG = "RecipeDatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "recipeManager";

    // Table Names
    private static final String TABLE_RECIPE = "tablerecipe";

    // NOTES Table - column nmaes
    private static final String KEY_ID = "id";
    private static final String KEY_RECIPE_NAME = "recipe_name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_RECIPE_IMAGE = "recipe_image";
    private static final String KEY_RECIPE_ITEM_TYPE = "recipe_item_type";
    private static final String KEY_RECIPE_PROMOTIONAL = "recipe_promotional";
    private static final String KEY_STATUS = "status";


    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_RECIPE = "CREATE TABLE "
            + TABLE_RECIPE + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_RECIPE_NAME
            + " TEXT," + KEY_DESCRIPTION
            + " TEXT," + KEY_RECIPE_IMAGE
            + " INTEGER," + KEY_RECIPE_ITEM_TYPE
            + " INTEGER," + KEY_RECIPE_PROMOTIONAL
            + " INTEGER," + KEY_STATUS + " INTEGER" + ")";

    private static DatabaseHelper mInstance = null;

    /**
     * constructor should be private to prevent direct instantiation.
     * make call to static factory method "getInstance()" instead.
     */
    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_RECIPE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPE);
        onCreate(db);
    }

    public long createRecipe(RecipeModel todo, long[] tag_ids) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_RECIPE_NAME, todo.recipeName);
        values.put(KEY_DESCRIPTION, todo.recipeDescription);
        values.put(KEY_RECIPE_IMAGE, todo.recipeImage);
        values.put(KEY_RECIPE_PROMOTIONAL, todo.recipePromotional);
        values.put(KEY_STATUS, STATUS_AVAILABLE);
        // insert row
        long todo_id = db.insert(TABLE_RECIPE, null, values);

        return todo_id;
    }

    public void createRecipes(ArrayList<RecipeModel> allRecipeModels) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (RecipeModel recipeModel : allRecipeModels) {
            ContentValues values = new ContentValues();
            values.put(KEY_RECIPE_NAME, recipeModel.recipeName);
            values.put(KEY_DESCRIPTION, recipeModel.recipeDescription);
            values.put(KEY_RECIPE_ITEM_TYPE, recipeModel.itemType);
            values.put(KEY_RECIPE_IMAGE, recipeModel.recipeImage);
            values.put(KEY_RECIPE_PROMOTIONAL, recipeModel.recipePromotional);
            values.put(KEY_STATUS, STATUS_AVAILABLE);
            // insert row
            db.insert(TABLE_RECIPE, null, values);
        }
    }

    /*
     * get single recipe
     */
    public ArrayList<RecipeModel> getAllRecipes() {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_RECIPE + " WHERE "
                + KEY_STATUS + " = " + STATUS_AVAILABLE;

        Log.e(LOG, selectQuery);
        ArrayList<RecipeModel> recipeModels = new ArrayList<>();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            while (c.moveToNext()) {
                RecipeModel recipeModel = new RecipeModel();
                recipeModel.id = c.getInt(c.getColumnIndex(KEY_ID));
                recipeModel.recipeName = c.getString(c.getColumnIndex(KEY_RECIPE_NAME));
                recipeModel.recipeDescription = c.getString(c.getColumnIndex(KEY_DESCRIPTION));
                recipeModel.recipeImage = c.getInt(c.getColumnIndex(KEY_RECIPE_IMAGE));
                recipeModel.itemType = c.getInt(c.getColumnIndex(KEY_RECIPE_ITEM_TYPE));
                recipeModel.recipePromotional = c.getInt(c.getColumnIndex(KEY_RECIPE_PROMOTIONAL));
                recipeModels.add(recipeModel);
            }
        return recipeModels;
    }

    /*
     * getting recipe count
     */
    public int getRecipeCount() {
        String countQuery = "SELECT  * FROM " + TABLE_RECIPE + " WHERE "
                + KEY_STATUS + " = " + STATUS_AVAILABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /*
     * Updating a recipe
     */
    public int updateToDo(RecipeModel todo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STATUS, STATUS_UNAVAILABLE);

        // updating row
        return db.update(TABLE_RECIPE, values, KEY_ID + " = ?",
                new String[]{String.valueOf(todo.id)});
    }

    /*
     * Deleting a recipe
     */
    public void deleteRecipe(long tado_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPE, KEY_ID + " = ?",
                new String[]{String.valueOf(tado_id)});
    }


    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
        mInstance = null;
    }

}
