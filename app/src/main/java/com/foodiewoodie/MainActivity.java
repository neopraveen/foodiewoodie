package com.foodiewoodie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.foodiewoodie.constant.Constants;
import com.foodiewoodie.dbhelper.DatabaseHelper;
import com.foodiewoodie.fragment.RecipeDescriptionFragment;
import com.foodiewoodie.fragment.RecipeListFragment;

public class MainActivity extends AppCompatActivity {
    ToolbarTextUpdater toolbarTextUpdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        addRecipeListFragment();
        initLocalBroadcastReceiver();
    }

    private void initLocalBroadcastReceiver() {
        toolbarTextUpdater = new ToolbarTextUpdater();
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(Constants.HEADER_TEXT);
        filter.addAction(Constants.SNACKBAR_SHOW);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(toolbarTextUpdater, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(toolbarTextUpdater);
    }

    public void addRecipeListFragment() {
        RecipeListFragment recipeListFragment = RecipeListFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.full_container_main, recipeListFragment);
//        addToBackStack(transaction, recipeListFragment);
        transaction.commit();
    }

    private void addToBackStack(FragmentTransaction transaction, Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        transaction.addToBackStack(backStateName);
    }

    public void openDescriptionFragment(int position) {
        RecipeDescriptionFragment recipeDescriptionFragment = RecipeDescriptionFragment.newInstance(position);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.full_container_main, recipeDescriptionFragment);
        addToBackStack(transaction, recipeDescriptionFragment);
        transaction.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DatabaseHelper.getInstance(getApplicationContext()).closeDB();
    }

    class ToolbarTextUpdater extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            final String headerText = intent.getStringExtra(Constants.DATA);
            Log.e("DATA", "Broadcast received - " + headerText);
            if (action.equals(Constants.HEADER_TEXT)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) findViewById(R.id.toolbar_text)).setText(headerText);
                    }
                });
            } else if (action.equals(Constants.SNACKBAR_SHOW)) {
                Snackbar.make(findViewById(R.id.toolbar), headerText, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }
}
