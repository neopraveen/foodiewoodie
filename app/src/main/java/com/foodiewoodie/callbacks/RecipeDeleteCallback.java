package com.foodiewoodie.callbacks;

import com.foodiewoodie.models.RecipeModel;

/**
 * Created by Praveen on 1/25/2017.
 */

public interface RecipeDeleteCallback {
    void recipeDeleted(RecipeModel recipeModel);
}
