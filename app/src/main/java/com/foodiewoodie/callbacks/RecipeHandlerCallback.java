package com.foodiewoodie.callbacks;


import com.foodiewoodie.models.RecipeModel;

import java.util.ArrayList;

/**
 * Created by Praveen on 1/24/2017.
 */

public interface RecipeHandlerCallback {
    void moreRecipes();
}
