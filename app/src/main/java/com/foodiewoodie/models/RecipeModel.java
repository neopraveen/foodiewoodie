package com.foodiewoodie.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.foodiewoodie.R;

/**
 * Created by Praveen on 1/23/2017.
 */

public class RecipeModel implements Parcelable {
    public static final int ITEM_TYPE_VEG = 0;
    public static final int ITEM_TYPE_NON_VEG = 1;
    public static final int NEW = 2;
    public static final int POPULAR = 1;
    public static final int NORMAL = 0;
    public static final Creator<RecipeModel> CREATOR = new Creator<RecipeModel>() {
        @Override
        public RecipeModel createFromParcel(Parcel in) {
            return new RecipeModel(in);
        }

        @Override
        public RecipeModel[] newArray(int size) {
            return new RecipeModel[size];
        }
    };
    public int id;
    public String recipeName;
    public String recipeDescription;
    public int recipeImage;
    public int itemType;
    public int recipePromotional;

    protected RecipeModel(Parcel in) {
        id = in.readInt();
        recipeName = in.readString();
        recipeDescription = in.readString();
        recipeImage = in.readInt();
        itemType = in.readInt();
        recipePromotional = in.readInt();
    }

    public RecipeModel() {

    }

    public int getItemTypeIcon() {
        if (itemType == ITEM_TYPE_VEG) {
            return R.drawable.veg;
        } else {
            return R.drawable.non_veg;
        }
    }

    public int getItemTypeText() {
        if (itemType == ITEM_TYPE_VEG) {
            return R.string.veg_text;
        } else {
            return R.string.non_veg_text;
        }
    }

    public int getPromotionalTypeIcon() {
        if (recipePromotional == NEW) {
            return R.drawable.new_rbn;
        } else if (recipePromotional == POPULAR) {
            return R.drawable.popular;
        } else {
            return 0;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(recipeName);
        parcel.writeString(recipeDescription);
        parcel.writeInt(recipeImage);
        parcel.writeInt(itemType);
        parcel.writeInt(recipePromotional);
    }
}
