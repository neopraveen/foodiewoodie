package com.foodiewoodie.wrapper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.foodiewoodie.R;


public class OpenSansTextView extends TextView {

    static Typeface MYRIAD_TTF = null;

    public OpenSansTextView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        setCustomTypeFace(context);
        TypedArray a = context.obtainStyledAttributes(attr, R.styleable.OpenSansTextView, defStyle, 0);
        boolean needToUnderline = a.getBoolean(0, false);
        a.recycle();
        if (needToUnderline)
            updateToUnderLinedText();
    }

    public OpenSansTextView(Context context, AttributeSet attr) {
        super(context, attr);
        setCustomTypeFace(context);
        TypedArray a = context.obtainStyledAttributes(attr, R.styleable.OpenSansTextView);
        boolean needToUnderline = a.getBoolean(0, false);
        a.recycle();
        if (needToUnderline)
            updateToUnderLinedText();
    }

    public OpenSansTextView(Context context) {
        super(context);
        setCustomTypeFace(context);
    }

    public static Typeface getNormalTf(Context context) {
        if (MYRIAD_TTF == null)
            MYRIAD_TTF = Typeface.createFromAsset(context.getAssets(),
                    "fonts/OpenSans-Regular.ttf");
        return MYRIAD_TTF;
    }

    public void updateToUnderLinedText() {
        String text = getText().toString();
        if (text != null && !text.equals("")) {
            SpannableString content = new SpannableString(text);
            content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
            setText(content);
        }
    }

    private void updateAsAttr(AttributeSet attrs) {
    }

    private void setCustomTypeFace(Context context) {
        try {
            if (!isInEditMode()) {
                setTypeface(getNormalTf(context), Typeface.BOLD);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
