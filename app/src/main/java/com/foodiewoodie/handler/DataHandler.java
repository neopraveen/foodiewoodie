package com.foodiewoodie.handler;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;

import com.foodiewoodie.R;
import com.foodiewoodie.callbacks.RecipeHandlerCallback;
import com.foodiewoodie.constant.Constants;
import com.foodiewoodie.dbhelper.DatabaseHelper;
import com.foodiewoodie.models.RecipeModel;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Praveen on 1/24/2017.
 */

public class DataHandler {
    private static DataHandler instance;
    RecipeHandlerCallback recipeHandlerCallback;
    ArrayList<RecipeModel> allAvailableRecipies = new ArrayList<>();
    boolean processingRequest;
    Activity activity;
    DatabaseHelper databaseHelper;


    public DataHandler(Activity activity, final RecipeHandlerCallback recipeHandlerCallback) {
        this.activity = activity;
        databaseHelper = DatabaseHelper.getInstance(activity.getApplicationContext());
        initAllAvailableData(activity);
        this.recipeHandlerCallback = recipeHandlerCallback;
        if (databaseHelper.getRecipeCount() == 0)
            loadMoreRecipes();                                     // For first time
    }

    public boolean isProcessingRequest() {
        return processingRequest;
    }

    public boolean checkAndGetMoreRecipes(int availableRecipesCount, int currentIndex) {
        if (!processingRequest && ((availableRecipesCount - currentIndex) < 4)) {
            loadMoreRecipes();
            return true;
        }
        return false;
    }

    public void loadMoreRecipes() {
        processingRequest = true;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<RecipeModel> moreRecipes = getMoreRecipes();
                        databaseHelper.createRecipes(moreRecipes);
                        processingRequest = false;
                        recipeHandlerCallback.moreRecipes();
                    }
                });
            }
        }, Constants.NETWORK_DUMMY_WAIT_TIME);
    }

    public ArrayList<RecipeModel> getMoreRecipes() {
        ArrayList<RecipeModel> moreRecipes = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            moreRecipes.add(allAvailableRecipies.get(i >= allAvailableRecipies.size() ? i % allAvailableRecipies.size() : i));
        }
        return moreRecipes;
    }

    private void initAllAvailableData(Context context) {
        String[] recipeNames = context.getResources().getStringArray(R.array.recipe_name_list);
        String[] recipeDescription = context.getResources().getStringArray(R.array.recipe_description_list);
        TypedArray recipeImages = context.getResources().obtainTypedArray(R.array.recipe_images);
        int[] recipeType = context.getResources().getIntArray(R.array.recipe_type);
        int[] promotionType = context.getResources().getIntArray(R.array.promo_type);
        for (int i = 0; i < recipeNames.length; i++) {
            RecipeModel recipeModel = new RecipeModel();
            recipeModel.recipeName = recipeNames[i];
            recipeModel.recipeDescription = recipeDescription[i];
            recipeModel.recipeImage = recipeImages.getResourceId(i, -1);
            recipeModel.itemType = recipeType[i];
            recipeModel.recipePromotional = promotionType[i];
            allAvailableRecipies.add(recipeModel);
        }

    }

}
