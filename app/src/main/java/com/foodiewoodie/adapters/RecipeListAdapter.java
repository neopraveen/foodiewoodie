package com.foodiewoodie.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodiewoodie.MainActivity;
import com.foodiewoodie.R;
import com.foodiewoodie.callbacks.RecipeHandlerCallback;
import com.foodiewoodie.constant.Constants;
import com.foodiewoodie.dbhelper.DatabaseHelper;
import com.foodiewoodie.handler.DataHandler;
import com.foodiewoodie.models.RecipeModel;

import java.util.ArrayList;

/**
 * Created by Praveen on 1/23/2017.
 */

public class RecipeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecipeHandlerCallback, View.OnClickListener {

    private static final int TYPE_LOADING = 0;
    private static final int TYPE_DATA = 1;
    private final Activity activity;
    ArrayList<RecipeModel> recipeModelList = new ArrayList<>();
    DataHandler dataHandler;

    public RecipeListAdapter(Activity activity) {
        this.activity = activity;
        dataHandler = new DataHandler(activity, this);
        recipeModelList = DatabaseHelper.getInstance(activity.getApplicationContext()).getAllRecipes();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                postToUpdatePageNumber();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewType == TYPE_DATA) {
            View view = layoutInflater.inflate(R.layout.content_recipe, parent, false);
            view.setOnClickListener(this);
            RecipeListHolder holder = new RecipeListHolder(view);
            return holder;
        } else {
            View view = layoutInflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_DATA) {
            holder.itemView.setTag(position);
            RecipeModel recipeModel = recipeModelList.get(position);
            ((RecipeListHolder) holder).recipeName.setText(recipeModel.recipeName);
            ((RecipeListHolder) holder).recipeImage.setImageResource(recipeModel.recipeImage);
            ((RecipeListHolder) holder).vegNonVegIcon.setImageResource(recipeModel.getItemTypeIcon());
            ((RecipeListHolder) holder).recipePromo.setImageResource(recipeModel.getPromotionalTypeIcon());
        }
        if (dataHandler.checkAndGetMoreRecipes(recipeModelList.size(), position)) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == recipeModelList.size())
            return TYPE_LOADING;
        return TYPE_DATA;
    }

    @Override
    public int getItemCount() {
        return recipeModelList.size() + (dataHandler.isProcessingRequest() ? 1 : 0);
    }

    @Override
    public void moreRecipes() {
        recipeModelList.clear();
        recipeModelList = DatabaseHelper.getInstance(activity.getApplicationContext()).getAllRecipes();
        postToUpdatePageNumber();
        notifyDataSetChanged();
    }

    private void postToUpdatePageNumber() {
        String toolbarPageText = "Total - " + recipeModelList.size();
        Intent intent = new Intent();
        intent.setAction(Constants.HEADER_TEXT);
        intent.putExtra(Constants.DATA, toolbarPageText);
        LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
        Log.e("DATA", "Broadcast sent - " + toolbarPageText);
    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
        ((MainActivity) activity).openDescriptionFragment(position);
    }

    public class RecipeListHolder extends RecyclerView.ViewHolder {
        ImageView recipeImage;
        TextView recipeName;
        ImageView recipePromo;
        ImageView vegNonVegIcon;

        public RecipeListHolder(View itemView) {
            super(itemView);
            recipeImage = (ImageView) itemView.findViewById(R.id.recipeImage);
            recipeName = (TextView) itemView.findViewById(R.id.recipeName);
            vegNonVegIcon = (ImageView) itemView.findViewById(R.id.veg_non_veg_icon);
            recipePromo = (ImageView) itemView.findViewById(R.id.recipePromo);
        }

    }

    public class LoadingHolder extends RecyclerView.ViewHolder {
        public LoadingHolder(View itemView) {
            super(itemView);
        }
    }
}
