package com.foodiewoodie.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.foodiewoodie.callbacks.RecipeDeleteCallback;
import com.foodiewoodie.constant.Constants;
import com.foodiewoodie.dbhelper.DatabaseHelper;
import com.foodiewoodie.fragment.RecipeDescriptionPageFragment;
import com.foodiewoodie.models.RecipeModel;

import java.util.ArrayList;

/**
 * Created by Praveen on 1/25/2017.
 */

public class RecipeDescriptionPagerAdapter extends FragmentStatePagerAdapter implements RecipeDeleteCallback {
    Activity activity;
    ArrayList<RecipeModel> recipeModelArrayList;
    ViewPager viewPager;
    int currentPosition = 0;

    public RecipeDescriptionPagerAdapter(FragmentManager fm, Activity activity, ViewPager viewPager) {
        super(fm);
        this.activity = activity;
        this.viewPager = viewPager;
        this.recipeModelArrayList = DatabaseHelper.getInstance(activity.getApplicationContext()).getAllRecipes();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        currentPosition = position;
        return super.getPageTitle(position);
    }

    @Override
    public Fragment getItem(int position) {
        RecipeDescriptionPageFragment recipeDescriptionPageFragment = RecipeDescriptionPageFragment.newInstance(recipeModelArrayList.get(position));
        recipeDescriptionPageFragment.setDeletionListener(this);
        postToUpdatePageNumber();
        return recipeDescriptionPageFragment;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return recipeModelArrayList.size();
    }

    private void postToUpdatePageNumber() {
        String toolbarPageText = (viewPager.getCurrentItem() + 1) + "/" + recipeModelArrayList.size();
        Intent intent = new Intent();
        intent.setAction(Constants.HEADER_TEXT);
        intent.putExtra(Constants.DATA, toolbarPageText);
        LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
        Log.e("DATA", "Broadcast sent - " + toolbarPageText);
    }

    @Override
    public void recipeDeleted(RecipeModel recipeModel) {
        DatabaseHelper.getInstance(activity.getApplicationContext()).deleteRecipe(recipeModel.id);
        showMessage(recipeModel);
        recipeModelArrayList.clear();
        recipeModelArrayList = DatabaseHelper.getInstance(activity.getApplicationContext()).getAllRecipes();
        postToUpdatePageNumber();
        notifyDataSetChanged();
    }

    private void showMessage(RecipeModel recipeModel) {
        Intent intent = new Intent();
        intent.setAction(Constants.SNACKBAR_SHOW);
        intent.putExtra(Constants.DATA, "Deleted recipe - " + recipeModel.recipeName);
        LocalBroadcastManager.getInstance(activity.getApplicationContext()).sendBroadcast(intent);
        Log.e("DATA", "Broadcast sent - " + "Deleted recipe - " + recipeModel.recipeName);
    }
}
