package com.foodiewoodie.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foodiewoodie.R;
import com.foodiewoodie.callbacks.RecipeDeleteCallback;
import com.foodiewoodie.models.RecipeModel;

/**
 * Created by Praveen on 1/25/2017.
 */

public class RecipeDescriptionPageFragment extends Fragment {
    private static final String DATA = "DATA";
    RecipeModel recipeModel;
    RecipeDeleteCallback recipeDeleteCallback;

    public static RecipeDescriptionPageFragment newInstance(RecipeModel recipeModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(DATA, recipeModel);
        RecipeDescriptionPageFragment recipeDescriptionPageFragment = new RecipeDescriptionPageFragment();
        recipeDescriptionPageFragment.setArguments(bundle);
        return recipeDescriptionPageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null)
            recipeModel = bundle.getParcelable(DATA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_recipe_description, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initListeners();
    }

    private void initViews() {
        ((ImageView) getView().findViewById(R.id.recipeImage)).setImageResource(recipeModel.recipeImage);
        ((ImageView) getView().findViewById(R.id.recipePromo)).setImageResource(recipeModel.getPromotionalTypeIcon());
        ((ImageView) getView().findViewById(R.id.veg_non_veg_icon)).setImageResource(recipeModel.getItemTypeIcon());
        ((TextView) getView().findViewById(R.id.recipeName)).setText(recipeModel.recipeName);
        ((TextView) getView().findViewById(R.id.description)).setText(recipeModel.recipeDescription);
    }

    private void initListeners() {
        getView().findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recipeDeleteCallback.recipeDeleted(recipeModel);
            }
        });
    }

    public void setDeletionListener(RecipeDeleteCallback recipeDeleteCallback) {
        this.recipeDeleteCallback = recipeDeleteCallback;
    }
}
