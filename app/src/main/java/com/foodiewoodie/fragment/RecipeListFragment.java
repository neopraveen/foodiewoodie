package com.foodiewoodie.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodiewoodie.R;
import com.foodiewoodie.adapters.RecipeListAdapter;

/**
 * Created by Praveen on 1/23/2017.
 */

public class RecipeListFragment extends Fragment {

    public static RecipeListFragment newInstance() {
        return new RecipeListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_recipe_list, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecipeList();
    }

    private void initRecipeList() {
        RecyclerView recipeRecyclerView = (RecyclerView) getView().findViewById(R.id.recipe_list);
        recipeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recipeRecyclerView.setAdapter(new RecipeListAdapter(getActivity()));
    }
}
