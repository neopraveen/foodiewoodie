package com.foodiewoodie.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foodiewoodie.R;
import com.foodiewoodie.adapters.RecipeDescriptionPagerAdapter;

/**
 * Created by Praveen on 1/25/2017.
 */

public class RecipeDescriptionFragment extends Fragment {
    private static final String DATA = "data";
    int selectedPlace;

    public static RecipeDescriptionFragment newInstance(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(DATA, position);
        RecipeDescriptionFragment recipeDescriptionPageFragment = new RecipeDescriptionFragment();
        recipeDescriptionPageFragment.setArguments(bundle);
        return recipeDescriptionPageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null)
            selectedPlace = bundle.getInt(DATA);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_recipe_description, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initPager();
    }

    private void initPager() {
        ViewPager recipeDescriptionPager = (ViewPager) getView().findViewById(R.id.recipe_pager);
        RecipeDescriptionPagerAdapter recipeDescriptionPagerAdapter = new RecipeDescriptionPagerAdapter(getFragmentManager(), getActivity(), recipeDescriptionPager);
        recipeDescriptionPager.setAdapter(recipeDescriptionPagerAdapter);
        recipeDescriptionPager.setCurrentItem(selectedPlace);
    }
}
